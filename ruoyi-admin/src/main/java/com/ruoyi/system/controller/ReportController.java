package com.ruoyi.system.controller;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.ProjectSumModel;
import com.ruoyi.system.mapper.SystemReportMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/system/report")
public class ReportController {
    private String prefix = "system/report";
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    private Calendar scb = Calendar.getInstance();
    @Autowired
    private SystemReportMapper systemReportMapper;

    @GetMapping("/projectLastWeekSummary")
    public String projectLastWeekSummary(ModelMap mmap) {
        int dow = scb.get(Calendar.DAY_OF_WEEK);
        boolean showThisWeek = false;
        if (dow == Calendar.FRIDAY || dow == Calendar.SATURDAY) { //周五、周六显示当周统计
            showThisWeek = true;
        }
        Calendar sc = Calendar.getInstance();
        sc.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        sc.add(Calendar.DAY_OF_MONTH, showThisWeek ? -7 : -14);
        Date sd = sc.getTime();
        Calendar ec = Calendar.getInstance();
        ec.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        ec.add(Calendar.DAY_OF_MONTH, showThisWeek ? 0 : -7);
        Date ed = ec.getTime();
        ec.add(Calendar.DATE, -1);
        Date showED = ec.getTime();
        String sdStr = format.format(sd);
        String edStr = format.format(ed);
        List<ProjectSumModel> projectLastWeekSummary = systemReportMapper.selectProjectLastWeekSummary(sdStr, edStr, false);
        mmap.put("startDate", sdStr);
        mmap.put("endDate", edStr);
        mmap.put("report", projectLastWeekSummary);
        return prefix + "/projectLastWeekSummary";
    }

    @GetMapping("/projectSummary")
    public String projectSummary(ModelMap mmap) {
        List<ProjectSumModel> projectCostSummary = systemReportMapper.selectProjectCostSummary(false);
        mmap.put("report", projectCostSummary);
        return prefix + "/projectSummary";
    }

    @GetMapping("/notReportUserList")
    public String notReportUserList(ModelMap mmap){

        scb.setTime(new Date());
        int dow = scb.get(Calendar.DAY_OF_WEEK);
        boolean showThisWeek = false;
        if (dow == Calendar.FRIDAY || dow == Calendar.SATURDAY) { //周五、周六显示当周统计
            showThisWeek = true;
        }
        Calendar sc = Calendar.getInstance();
        sc.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        sc.add(Calendar.DAY_OF_MONTH, showThisWeek ? -7 : -14);
        Date sd = sc.getTime();
        Calendar ec = Calendar.getInstance();
        ec.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        ec.add(Calendar.DAY_OF_MONTH, showThisWeek ? 0 : -7);
        Date ed = ec.getTime();
        ec.add(Calendar.DATE, -1);
        Date showED = ec.getTime();
        String sdStr = format.format(sd);
        String edStr = format.format(ed);
        List<SysUser> ls = systemReportMapper.selectNotReportedUserList(sdStr, edStr);
        mmap.put("report", ls);
        return prefix + "/notReportUserList";
    }
}
