package com.ruoyi.system.controller;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.TblContract;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.service.ITblContractService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TblProject;
import com.ruoyi.system.service.ITblProjectService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 项目Controller
 *
 * @author ruoyi
 * @date 2021-02-09
 */
@Controller
@RequestMapping("/system/project")
public class TblProjectController extends BaseController {
    private String prefix = "system/project";

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ITblProjectService tblProjectService;

    @Autowired
    private ITblContractService tblContractService;

    @RequiresPermissions("system:project:view")
    @GetMapping()
    public String project(ModelMap mmap) {
        mmap.put("users", userService.selectAvailableUserList(new SysUser()));
        return prefix + "/project";
    }

    /**
     * 查询项目列表
     */
    @RequiresPermissions("system:project:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TblProject tblProject) {
        startPage();
        List<TblProject> list = tblProjectService.selectTblProjectList(tblProject);
        return getDataTable(list);
    }

    /**
     * 导出项目列表
     */
    @RequiresPermissions("system:project:export")
    @Log(title = "项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TblProject tblProject) {
        List<TblProject> list = tblProjectService.selectTblProjectList(tblProject);
        ExcelUtil<TblProject> util = new ExcelUtil<TblProject>(TblProject.class);
        return util.exportExcel(list, "project");
    }

    /**
     * 新增项目
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("leaders", userService.selectAvailableUserList(new SysUser()));
        mmap.put("contracts", tblContractService.selectTblContractList(new TblContract()));
        return prefix + "/add";
    }

    /**
     * 新增保存项目
     */
    @RequiresPermissions("system:project:add")
    @Log(title = "项目", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TblProject tblProject) {
        tblProject.setCreateBy(ShiroUtils.getLoginName());
        if (tblProject.getStartDate() != null && tblProject.getEndDate() != null &&
                tblProject.getStartDate().after(tblProject.getEndDate())) {
            return error("添加项目失败，起始时间大于结束时间");
        }
        return toAjax(tblProjectService.insertTblProject(tblProject));
    }

    /**
     * 修改项目
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        TblProject tblProject = tblProjectService.selectTblProjectById(id);
        mmap.put("tblProject", tblProject);
        mmap.put("leaders", userService.selectAvailableUserList(new SysUser()));
        mmap.put("contracts", tblContractService.selectTblContractList(new TblContract()));
        return prefix + "/edit";
    }

    /**
     * 修改保存项目
     */
    @RequiresPermissions("system:project:edit")
    @Log(title = "项目", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TblProject tblProject) {
        tblProject.setUpdateBy(ShiroUtils.getLoginName());
        if (tblProject.getStartDate() != null && tblProject.getEndDate() != null &&
                tblProject.getStartDate().after(tblProject.getEndDate())) {
            return error("修改项目失败，起始时间大于结束时间");
        }
        return toAjax(tblProjectService.updateTblProject(tblProject));
    }

    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        TblProject tblProject = tblProjectService.selectTblProjectById(id);
        SysUser leader = null;//new SysUser();
        TblContract contract = null;
        new TblContract();
        if (tblProject != null) {
            leader = userService.selectUserById(tblProject.getLeaderId());
            contract = tblContractService.selectTblContractById(tblProject.getContractId());
        }

        mmap.put("tblProject", tblProject);
        mmap.put("leader", leader != null ? leader : new SysUser());
        mmap.put("contract", contract != null ? contract : new TblContract());
        return prefix + "/detail";
    }

    @GetMapping("/authUser/{id}")
    public String authUser(@PathVariable("id") String id, ModelMap mmap) {
        TblProject tblProject = tblProjectService.selectTblProjectById(id);
        mmap.put("project", tblProject);
        mmap.put("users", tblProjectService.selectTblProjectUserList(id));
        return prefix + "/authUser";
    }

    @RequiresPermissions("system:user:add")
    @Log(title = "用户管理", businessType = BusinessType.GRANT)
    @PostMapping("/authUser/insert")
    @ResponseBody
    public AjaxResult insertAuthProject(String projectId, String[] mine) {
        tblProjectService.insertAuthUser(projectId, mine);
        return success();
    }

    /**
     * 删除项目
     */
    @RequiresPermissions("system:project:remove")
    @Log(title = "项目", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(tblProjectService.deleteTblProjectByIds(ids));
    }

    @GetMapping("/projectfiles/{id}")
    public String contractfiles(@PathVariable("id") String id, ModelMap mmap) {
        TblProject tblProject = tblProjectService.selectTblProjectById(id);
        mmap.put("tblProject", tblProject);
        return prefix + "/projectfiles";
    }
}
