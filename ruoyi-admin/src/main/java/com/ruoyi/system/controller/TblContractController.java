package com.ruoyi.system.controller;

import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.system.domain.TblContractSettlement;
import com.ruoyi.system.domain.TblProject;
import com.ruoyi.system.domain.TblUploadFile;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.service.ITblUploadFileService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TblContract;
import com.ruoyi.system.service.ITblContractService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 合同Controller
 *
 * @author ruoyi
 * @date 2022-02-24
 */
@Controller
@RequestMapping("/system/contract")
public class TblContractController extends BaseController {
    private String prefix = "system/contract";

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ITblContractService tblContractService;

    @Autowired
    private ITblUploadFileService tblUploadFileService;

    @RequiresPermissions("system:contract:view")
    @GetMapping()
    public String contract(ModelMap mmap) {
        mmap.put("users", userService.selectAvailableUserList(new SysUser()));
        return prefix + "/contract";
    }

    /**
     * 查询合同列表
     */
    @RequiresPermissions("system:contract:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TblContract tblContract) {
        startPage();
        List<TblContract> list = tblContractService.selectTblContractList(tblContract);
        return getDataTable(list);
    }

    /**
     * 导出合同列表
     */
    @RequiresPermissions("system:contract:export")
    @Log(title = "合同", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TblContract tblContract) {
        List<TblContract> list = tblContractService.selectTblContractList(tblContract);
        ExcelUtil<TblContract> util = new ExcelUtil<TblContract>(TblContract.class);
        return util.exportExcel(list, "contract");
    }

    /**
     * 新增合同
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("leaders", userService.selectAvailableUserList(new SysUser()));
        return prefix + "/add";
    }

    /**
     * 新增保存合同
     */
    @RequiresPermissions("system:contract:add")
    @Log(title = "合同", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TblContract tblContract) {
        tblContract.setCreateBy(ShiroUtils.getLoginName());
        if (tblContract.getStartDate().after(tblContract.getEndDate())) {
            return error("添加合同失败，起始时间大于结束时间");
        }
        return toAjax(tblContractService.insertTblContract(tblContract));
    }

    /**
     * 修改合同
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        TblContract tblContract = tblContractService.selectTblContractById(id);
        mmap.put("tblContract", tblContract);
        mmap.put("leaders", userService.selectAvailableUserList(new SysUser()));
        return prefix + "/edit";
    }

    /**
     * 修改保存合同
     */
    @RequiresPermissions("system:contract:edit")
    @Log(title = "合同", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TblContract tblContract) {
        tblContract.setUpdateBy(ShiroUtils.getLoginName());
        if (tblContract.getStartDate() != null && tblContract.getEndDate() != null && tblContract.getStartDate().after(tblContract.getEndDate())) {
            return error("修改合同失败，起始时间大于结束时间");
        }
        return toAjax(tblContractService.updateTblContract(tblContract));
    }

    /**
     * 修改项目
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        TblContract tblContract = tblContractService.selectTblContractById(id);
        mmap.put("tblContract", tblContract);
        mmap.put("leaders", userService.selectAvailableUserList(new SysUser()));
        return prefix + "/detail";
    }

    /**
     * 删除合同
     */
    @RequiresPermissions("system:contract:remove")
    @Log(title = "合同", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(tblContractService.deleteTblContractByIds(ids));
    }


    @GetMapping("/contractfiles/{id}")
    public String contractFiles(@PathVariable("id") String id, ModelMap mmap) {
        TblContract tblContract = tblContractService.selectTblContractById(id);
        mmap.put("tblContract", tblContract);
        return prefix + "/contractfiles";
    }


}
