package com.ruoyi.system.controller;

import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TblUploadFile;
import com.ruoyi.system.service.ITblUploadFileService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件关联Controller
 * 
 * @author ruoyi
 * @date 2022-03-08
 */
@Controller
@RequestMapping("/system/file")
public class TblUploadFileController extends BaseController
{
    private String prefix = "system/file";

    @Autowired
    private ITblUploadFileService tblUploadFileService;

    /**
     * 删除文件关联
     */
    @RequiresPermissions("system:file:remove")
    @Log(title = "文件关联", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(@RequestBody String ids)
    {
        return toAjax(tblUploadFileService.deleteTblUploadFileByIds(ids));
    }

    @PostMapping("/uploadfile")
    @ResponseBody
    public AjaxResult uploadfile(@RequestParam("file") MultipartFile file, TblUploadFile fileInfo) {
        String relType = fileInfo.getRelType();;
        String fileBasePath = RuoYiConfig.getUploadPath() + "/" + relType;
        String filePath = null;
        try {
            filePath = FileUploadUtils.upload(fileBasePath, file);
        } catch (Exception e) {
            return error("文件上传失败");
        }
        fileInfo.setFilePath(filePath);
        fileInfo.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(tblUploadFileService.insertTblUploadFile(fileInfo));
    }

    @PostMapping("/queryfilelist")
    @ResponseBody
    public AjaxResult queryfilelist(@RequestBody TblUploadFile fileInfo){
        return  AjaxResult.success(tblUploadFileService.selectTblUploadFileList(fileInfo));
    }
}
