package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.CntSettleSummaryModel;
import com.ruoyi.system.domain.TblContractSettlement;

/**
 * 合同收款Mapper接口
 * 
 * @author ruoyi
 * @date 2022-03-01
 */
public interface TblContractSettlementMapper 
{
    /**
     * 查询合同收款
     * 
     * @param id 合同收款ID
     * @return 合同收款
     */
    public TblContractSettlement selectTblContractSettlementById(String id);

    /**
     * 查询合同收款列表
     * 
     * @param tblContractSettlement 合同收款
     * @return 合同收款集合
     */
    public List<TblContractSettlement> selectTblContractSettlementList(TblContractSettlement tblContractSettlement);

    /**
     * 新增合同收款
     * 
     * @param tblContractSettlement 合同收款
     * @return 结果
     */
    public int insertTblContractSettlement(TblContractSettlement tblContractSettlement);


    /**
     * 新增合同收款
     *
     * @param tblContractSettlement 合同收款
     * @return 结果
     */
    public int insertTblContractSettlementList(List<TblContractSettlement> tblContractSettlements);

    /**
     * 修改合同收款
     * 
     * @param tblContractSettlement 合同收款
     * @return 结果
     */
    public int updateTblContractSettlement(TblContractSettlement tblContractSettlement);

    /**
     * 删除合同收款
     * 
     * @param id 合同收款ID
     * @return 结果
     */
    public int deleteTblContractSettlementById(String id);

    /**
     * 批量删除合同收款
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTblContractSettlementByIds(String[] ids);

    public List<CntSettleSummaryModel>  selectContractSettleSummary(String[] ids);

    public List<CntSettleSummaryModel> selectContractPayedSummary(String[] ids);
    public List<CntSettleSummaryModel> selectContractPlanSettleSummary(String[] ids);
}
