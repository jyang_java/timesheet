package com.ruoyi.system.mapper;

import java.util.Date;
import java.util.List;

import com.ruoyi.system.domain.ProjectSumModel;
import com.ruoyi.system.domain.TblProject;
import com.ruoyi.system.domain.TblProjectCost;
import com.ruoyi.system.domain.UserLastWeekSumModel;
import org.apache.ibatis.annotations.Param;

/**
 * 工时Mapper接口
 * 
 * @author ruoyi
 * @date 2021-02-18
 */
public interface TblProjectCostMapper {
	/**
	 * 查询工时
	 * 
	 * @param id 工时ID
	 * @return 工时
	 */
	public TblProjectCost selectTblProjectCostById(TblProjectCost tblProjectCost);

	/**
	 * 查询工时列表
	 * 
	 * @param tblProjectCost 工时
	 * @return 工时集合
	 */
	public List<TblProjectCost> selectTblProjectCostList(TblProjectCost tblProjectCost);

	/**
	 * 统计工时
	 * 
	 * @param tblProjectCost 工时
	 * @return 工时集合
	 */
	public List<TblProjectCost> selectTblProjectCostSumMember(TblProjectCost tblProjectCost);

	public List<TblProjectCost> selectTblProjectCostSumProject(TblProjectCost tblProjectCost);

	/**
	 * 新增工时
	 * 
	 * @param tblProjectCost 工时
	 * @return 结果
	 */
	public int insertTblProjectCost(TblProjectCost tblProjectCost);

	/**
	 * 新增工时
	 * 
	 * @param tblProjectCost 工时
	 * @return 结果
	 */
	public int insertTblProjectCostList(List<TblProjectCost> list);

	/**
	 * 修改工时
	 * 
	 * @param tblProjectCost 工时
	 * @return 结果
	 */
	public int updateTblProjectCost(TblProjectCost tblProjectCost);

	public int updateTblProjectCostName(TblProject tblProject);

	/**
	 * 删除工时
	 * 
	 * @param id 工时ID
	 * @return 结果
	 */
	public int deleteTblProjectCostById(String id);

	/**
	 * 批量删除工时
	 * 
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	public int deleteTblProjectCostByIds(String[] ids);

	/**
	 * 查询最近统计时间
	 * 
	 * @param userId userId
	 * @return 最近统计时间
	 */
	public TblProjectCost selectTblProjectCostLastData(Long userId);

	public int updateUserUpdatetime(Long user);

	public int insertUserUpdatetime(Long user);


}
