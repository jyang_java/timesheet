package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TblUploadFile;

/**
 * 文件关联Mapper接口
 * 
 * @author ruoyi
 * @date 2022-03-08
 */
public interface TblUploadFileMapper 
{
    /**
     * 查询文件关联
     * 
     * @param id 文件关联ID
     * @return 文件关联
     */
    public TblUploadFile selectTblUploadFileById(String id);

    /**
     * 查询文件关联列表
     * 
     * @param tblUploadFile 文件关联
     * @return 文件关联集合
     */
    public List<TblUploadFile> selectTblUploadFileList(TblUploadFile tblUploadFile);

    /**
     * 新增文件关联
     * 
     * @param tblUploadFile 文件关联
     * @return 结果
     */
    public int insertTblUploadFile(TblUploadFile tblUploadFile);

    /**
     * 修改文件关联
     * 
     * @param tblUploadFile 文件关联
     * @return 结果
     */
    public int updateTblUploadFile(TblUploadFile tblUploadFile);

    /**
     * 删除文件关联
     * 
     * @param id 文件关联ID
     * @return 结果
     */
    public int deleteTblUploadFileById(String id);

    /**
     * 批量删除文件关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTblUploadFileByIds(String[] ids);
}
