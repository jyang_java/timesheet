package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.IndexProjectStatusSumModel;
import com.ruoyi.system.domain.ProjectSumModel;
import com.ruoyi.system.domain.UserLastWeekSumModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemReportMapper {
    public List<ProjectSumModel> selectProjectLastWeekSummary(@Param("startDate") String startDate , @Param("endDate") String endDate,@Param("queryTop") Boolean queryTop);

    public List<ProjectSumModel> selectProjectCostSummary(@Param("queryTop") Boolean queryTop);

    public UserLastWeekSumModel selectUserLastWeekSummary(@Param("startDate") String startDate , @Param("endDate") String endDate);

    public List<IndexProjectStatusSumModel> selectProjectSummaryByStatus();

    public List<SysUser> selectNotReportedUserList(@Param("startDate") String startDate , @Param("endDate") String endDate);
}
