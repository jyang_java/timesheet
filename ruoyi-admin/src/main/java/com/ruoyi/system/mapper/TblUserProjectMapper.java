package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TblProject;
import com.ruoyi.system.domain.TblUserProject;

/**
 * 项目Mapper接口
 * 
 * @author ruoyi
 * @date 2021-02-09
 */
public interface TblUserProjectMapper {
	public static String Insert_ALL = "all";
	public static String Clear_All = "clearAll";
	/**
	 * 查询项目
	 * 
	 * @param id 项目ID
	 * @return 项目
	 */
	public String[] selectTblUserProjectByUser(Long userId);

	public Long[] selectTblUserProjectByProject(String projectId);

	/**
	 * 新增项目
	 * 
	 * @param tblProject 项目
	 * @return 结果
	 */
	public int insertTblUserProject(List<TblUserProject> list);

	public int insertTblUserProjectAllProject(Long userId);
	public int insertTblUserProjectAllUser(String projectId);

	/**
	 * 删除项目
	 * 
	 * @param id 项目ID
	 * @return 结果
	 */
	public int deleteTblUserProjectByUser(Long userId);
	public int deleteTblUserProjectByProject(String projectId);

}
