package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.ContractSettleIndexModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TblContractMapper;
import com.ruoyi.system.domain.TblContract;
import com.ruoyi.system.service.ITblContractService;
import com.ruoyi.common.core.text.Convert;

/**
 * 合同Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-02-24
 */
@Service
public class TblContractServiceImpl implements ITblContractService 
{
    @Autowired
    private TblContractMapper tblContractMapper;

    /**
     * 查询合同
     * 
     * @param id 合同ID
     * @return 合同
     */
    @Override
    public TblContract selectTblContractById(String id)
    {
        return tblContractMapper.selectTblContractById(id);
    }

    /**
     * 查询合同列表
     * 
     * @param tblContract 合同
     * @return 合同
     */
    @Override
    public List<TblContract> selectTblContractList(TblContract tblContract)
    {
        return tblContractMapper.selectTblContractList(tblContract);
    }

    /**
     * 查询合同收款列表
     *
     * @param tblContract 合同
     * @return 合同
     */
    @Override
    public List<ContractSettleIndexModel> selectTblContractSettleList(TblContract tblContract)
    {
        return tblContractMapper.selectTblContractSettleList(tblContract);
    }

    /**
     * 新增合同
     * 
     * @param tblContract 合同
     * @return 结果
     */
    @Override
    public int insertTblContract(TblContract tblContract)
    {
        tblContract.setCreateTime(DateUtils.getNowDate());
        return tblContractMapper.insertTblContract(tblContract);
    }

    /**
     * 修改合同
     * 
     * @param tblContract 合同
     * @return 结果
     */
    @Override
    public int updateTblContract(TblContract tblContract)
    {
        tblContract.setUpdateTime(DateUtils.getNowDate());
        return tblContractMapper.updateTblContract(tblContract);
    }

    /**
     * 删除合同对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTblContractByIds(String ids)
    {
        return tblContractMapper.deleteTblContractByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除合同信息
     * 
     * @param id 合同ID
     * @return 结果
     */
    @Override
    public int deleteTblContractById(String id)
    {
        return tblContractMapper.deleteTblContractById(id);
    }
}
