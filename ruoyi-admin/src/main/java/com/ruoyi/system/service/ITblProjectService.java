package com.ruoyi.system.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.IndexProjectStatusSumModel;
import com.ruoyi.system.domain.TblProject;

/**
 * 项目Service接口
 * 
 * @author ruoyi
 * @date 2021-02-09
 */
public interface ITblProjectService {
	/**
	 * 查询项目
	 * 
	 * @param id 项目ID
	 * @return 项目
	 */
	public TblProject selectTblProjectById(String id);

	/**
	 * 查询项目列表
	 * 
	 * @param tblProject 项目
	 * @return 项目集合
	 */
	public List<TblProject> selectTblProjectList(TblProject tblProject);

	/**
	 * 新增项目
	 * 
	 * @param tblProject 项目
	 * @return 结果
	 */
	public int insertTblProject(TblProject tblProject);

	/**
	 * 修改项目
	 * 
	 * @param tblProject 项目
	 * @return 结果
	 */
	public int updateTblProject(TblProject tblProject);

	/**
	 * 批量删除项目
	 * 
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	public int deleteTblProjectByIds(String ids);

	/**
	 * 删除项目信息
	 * 
	 * @param id 项目ID
	 * @return 结果
	 */
	public int deleteTblProjectById(String id);

	public Map<String, Object> selectTblUserProjectList(Long userId);

	public Map<String, Object> selectTblProjectUserList(String id);

	public void insertAuthProject(Long userId, String[] mine);
	public void insertAuthUser(String projectId,String[] mine);

	public List<TblProject> selectMyTblProjectList(Long userId);
	public List<TblProject> selectPMOTblProjectList(Long userId);

	public List<TblProject> selectTblProjectListByUserProject(Long userId);

}
