package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TblUploadFileMapper;
import com.ruoyi.system.domain.TblUploadFile;
import com.ruoyi.system.service.ITblUploadFileService;
import com.ruoyi.common.core.text.Convert;

/**
 * 文件关联Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-03-08
 */
@Service
public class TblUploadFileServiceImpl implements ITblUploadFileService 
{
    @Autowired
    private TblUploadFileMapper tblUploadFileMapper;

    /**
     * 查询文件关联
     * 
     * @param id 文件关联ID
     * @return 文件关联
     */
    @Override
    public TblUploadFile selectTblUploadFileById(String id)
    {
        return tblUploadFileMapper.selectTblUploadFileById(id);
    }

    /**
     * 查询文件关联列表
     * 
     * @param tblUploadFile 文件关联
     * @return 文件关联
     */
    @Override
    public List<TblUploadFile> selectTblUploadFileList(TblUploadFile tblUploadFile)
    {
        return tblUploadFileMapper.selectTblUploadFileList(tblUploadFile);
    }

    /**
     * 新增文件关联
     * 
     * @param tblUploadFile 文件关联
     * @return 结果
     */
    @Override
    public int insertTblUploadFile(TblUploadFile tblUploadFile)
    {
        tblUploadFile.setCreateTime(DateUtils.getNowDate());
        return tblUploadFileMapper.insertTblUploadFile(tblUploadFile);
    }

    /**
     * 修改文件关联
     * 
     * @param tblUploadFile 文件关联
     * @return 结果
     */
    @Override
    public int updateTblUploadFile(TblUploadFile tblUploadFile)
    {
        return tblUploadFileMapper.updateTblUploadFile(tblUploadFile);
    }

    /**
     * 删除文件关联对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTblUploadFileByIds(String ids)
    {
        return tblUploadFileMapper.deleteTblUploadFileByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除文件关联信息
     * 
     * @param id 文件关联ID
     * @return 结果
     */
    @Override
    public int deleteTblUploadFileById(String id)
    {
        return tblUploadFileMapper.deleteTblUploadFileById(id);
    }
}
