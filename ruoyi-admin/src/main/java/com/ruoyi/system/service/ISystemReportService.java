package com.ruoyi.system.service;

import com.ruoyi.system.domain.IndexProjectStatusSumModel;
import com.ruoyi.system.domain.ProjectSumModel;
import com.ruoyi.system.domain.UserLastWeekSumModel;

import java.util.List;

public interface ISystemReportService {
    public List<ProjectSumModel> selectProjectLastWeekSummary(String startDate, String endDate, Boolean queryTop);

    public List<ProjectSumModel> selectProjectCostSummary(Boolean queryTop);

    public UserLastWeekSumModel selectUserLastWeekSummary(String startDate , String endDate);

    public List<IndexProjectStatusSumModel> selectProjectSummaryByStatus();
}
