package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.CntSettleSummaryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TblContractSettlementMapper;
import com.ruoyi.system.domain.TblContractSettlement;
import com.ruoyi.system.service.ITblContractSettlementService;
import com.ruoyi.common.core.text.Convert;

/**
 * 合同收款Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-03-01
 */
@Service
public class TblContractSettlementServiceImpl implements ITblContractSettlementService 
{
    @Autowired
    private TblContractSettlementMapper tblContractSettlementMapper;

    /**
     * 查询合同收款
     * 
     * @param id 合同收款ID
     * @return 合同收款
     */
    @Override
    public TblContractSettlement selectTblContractSettlementById(String id)
    {
        return tblContractSettlementMapper.selectTblContractSettlementById(id);
    }

    /**
     * 查询合同收款列表
     * 
     * @param tblContractSettlement 合同收款
     * @return 合同收款
     */
    @Override
    public List<TblContractSettlement> selectTblContractSettlementList(TblContractSettlement tblContractSettlement)
    {
        return tblContractSettlementMapper.selectTblContractSettlementList(tblContractSettlement);
    }

    /**
     * 新增合同收款
     * 
     * @param tblContractSettlement 合同收款
     * @return 结果
     */
    @Override
    public int insertTblContractSettlement(TblContractSettlement tblContractSettlement)
    {
        tblContractSettlement.setCreateTime(DateUtils.getNowDate());
        return tblContractSettlementMapper.insertTblContractSettlement(tblContractSettlement);
    }

    /**
     * 新增合同收款
     *
     * @param tblContractSettlement 合同收款
     * @return 结果
     */
    @Override
    public int insertTblContractSettlementList(List<TblContractSettlement> tblContractSettlements)
    {
        return tblContractSettlementMapper.insertTblContractSettlementList(tblContractSettlements);
    }

    /**
     * 修改合同收款
     * 
     * @param tblContractSettlement 合同收款
     * @return 结果
     */
    @Override
    public int updateTblContractSettlement(TblContractSettlement tblContractSettlement)
    {
        tblContractSettlement.setUpdateTime(DateUtils.getNowDate());
        return tblContractSettlementMapper.updateTblContractSettlement(tblContractSettlement);
    }

    /**
     * 删除合同收款对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTblContractSettlementByIds(String ids)
    {
        return tblContractSettlementMapper.deleteTblContractSettlementByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除合同收款信息
     * 
     * @param id 合同收款ID
     * @return 结果
     */
    @Override
    public int deleteTblContractSettlementById(String id)
    {
        return tblContractSettlementMapper.deleteTblContractSettlementById(id);
    }

    @Override
    public List<CntSettleSummaryModel>  selectContractSettleSummary(String[] ids)
    {
        return  tblContractSettlementMapper.selectContractSettleSummary(ids);
    }
    @Override
    public List<CntSettleSummaryModel> selectContractPayedSummary(String[] ids)
    {
        return  tblContractSettlementMapper.selectContractPayedSummary(ids);
    }
    @Override
    public List<CntSettleSummaryModel> selectContractPlanSettleSummary(String[] ids)
    {
        return  tblContractSettlementMapper.selectContractPlanSettleSummary(ids);
    }
}
