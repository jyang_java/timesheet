package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.system.domain.ProjectSumModel;
import com.ruoyi.system.domain.UserLastWeekSumModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.TblProjectCost;
import com.ruoyi.system.mapper.TblProjectCostMapper;
import com.ruoyi.system.service.ITblProjectCostService;

/**
 * 工时Service业务层处理
 *
 * @author ruoyi
 * @date 2021-02-18
 */
@Service
public class TblProjectCostServiceImpl implements ITblProjectCostService {
    @Autowired
    private TblProjectCostMapper tblProjectCostMapper;

    /**
     * 查询工时
     *
     * @param id 工时ID
     * @return 工时
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public TblProjectCost selectTblProjectCostById(TblProjectCost tblProjectCost) {
        return tblProjectCostMapper.selectTblProjectCostById(tblProjectCost);
    }

    /**
     * 查询工时列表
     *
     * @param tblProjectCost 工时
     * @return 工时
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<TblProjectCost> selectTblProjectCostList(TblProjectCost tblProjectCost) {
        return tblProjectCostMapper.selectTblProjectCostList(tblProjectCost);
    }

    /**
     * 新增工时
     *
     * @param tblProjectCost 工时
     * @return 结果
     */
    @Override
    public int insertTblProjectCost(TblProjectCost tblProjectCost) {
        tblProjectCost.setCreateTime(DateUtils.getNowDate());
        return tblProjectCostMapper.insertTblProjectCost(tblProjectCost);
    }

    /**
     * 新增工时
     *
     * @param tblProjectCost 工时
     * @return 结果
     */
    @Override
    public int insertTblProjectCostList(List<TblProjectCost> list) {
        if (tblProjectCostMapper.insertTblProjectCostList(list) > 0) {
            return updateUserUpdatetime(list.get(0).getUser());
        }
        return 0;
    }

    @Override
    public int updateUserUpdatetime(Long userId) {
        if (tblProjectCostMapper.updateUserUpdatetime(userId) == 0) {
            tblProjectCostMapper.insertUserUpdatetime(userId);
        }
        return 1;
    }

    /**
     * 修改工时
     *
     * @param tblProjectCost 工时
     * @return 结果
     */
    @Override
    public int updateTblProjectCost(TblProjectCost tblProjectCost) {
        tblProjectCost.setUpdateTime(DateUtils.getNowDate());
        return tblProjectCostMapper.updateTblProjectCost(tblProjectCost);
    }

    /**
     * 删除工时对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTblProjectCostByIds(String ids) {
        return tblProjectCostMapper.deleteTblProjectCostByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除工时信息
     *
     * @param id 工时ID
     * @return 结果
     */
    @Override
    public int deleteTblProjectCostById(String id) {
        return tblProjectCostMapper.deleteTblProjectCostById(id);
    }

    /**
     * 查询最近统计时间
     *
     * @param userId userId
     * @return 最近统计时间
     */
    @Override
    public TblProjectCost selectTblProjectCostLastData(Long userId) {
        return tblProjectCostMapper.selectTblProjectCostLastData(userId);
    }

    /**
     * 统计工时列表
     *
     * @param tblProjectCost 工时
     * @return 工时
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<TblProjectCost> selectTblProjectCostSumMember(TblProjectCost tblProjectCost) {
        return tblProjectCostMapper.selectTblProjectCostSumMember(tblProjectCost);
    }

    @Override
    public List<TblProjectCost> selectTblProjectCostSumProject(TblProjectCost tblProjectCost) {
        return tblProjectCostMapper.selectTblProjectCostSumProject(tblProjectCost);
    }

}
