package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.IndexProjectStatusSumModel;
import com.ruoyi.system.domain.ProjectSumModel;
import com.ruoyi.system.domain.UserLastWeekSumModel;
import com.ruoyi.system.mapper.SystemReportMapper;
import com.ruoyi.system.mapper.TblProjectCostMapper;
import com.ruoyi.system.service.ISystemReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemReportMapperImpl implements ISystemReportService {
    @Autowired
    private SystemReportMapper systemReportMapper;

    @Override
    public List<ProjectSumModel> selectProjectLastWeekSummary(String startDate, String endDate, Boolean queryTop) {
        return systemReportMapper.selectProjectLastWeekSummary(startDate, endDate, queryTop);
    }

    @Override
    public List<ProjectSumModel> selectProjectCostSummary(Boolean queryTop) {
        return systemReportMapper.selectProjectCostSummary(queryTop);
    }

    @Override
    public UserLastWeekSumModel selectUserLastWeekSummary(String startDate, String endDate) {
        return systemReportMapper.selectUserLastWeekSummary(startDate, endDate);
    }
    @Override
    public List<IndexProjectStatusSumModel> selectProjectSummaryByStatus(){return  systemReportMapper.selectProjectSummaryByStatus();}
}
