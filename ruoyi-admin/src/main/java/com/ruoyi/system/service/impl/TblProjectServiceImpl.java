package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.IndexProjectStatusSumModel;
import com.ruoyi.system.domain.query.UserIDQuery;
import com.ruoyi.system.service.ISysUserService;
import net.sf.jsqlparser.expression.operators.relational.OldOracleJoinBinaryExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.TblProject;
import com.ruoyi.system.domain.TblProjectCost;
import com.ruoyi.system.domain.TblUserProject;
import com.ruoyi.system.mapper.TblProjectCostMapper;
import com.ruoyi.system.mapper.TblProjectMapper;
import com.ruoyi.system.mapper.TblUserProjectMapper;
import com.ruoyi.system.service.ITblProjectService;

/**
 * 项目Service业务层处理
 *
 * @author ruoyi
 * @date 2021-02-09
 */
@Service
public class TblProjectServiceImpl implements ITblProjectService {
    @Autowired
    private ISysUserService userService;

    @Autowired
    private TblProjectMapper tblProjectMapper;

    @Autowired
    private TblProjectCostMapper tblProjectCostMapper;

    @Autowired
    private TblUserProjectMapper tblUserProjectMapper;

    /**
     * 查询项目
     *
     * @param id 项目ID
     * @return 项目
     */
    @Override
    public TblProject selectTblProjectById(String id) {
        return tblProjectMapper.selectTblProjectById(id);
    }

    /**
     * 查询项目列表
     *
     * @param tblProject 项目
     * @return 项目
     */
    @Override
    public List<TblProject> selectTblProjectList(TblProject tblProject) {
        return tblProjectMapper.selectTblProjectList(tblProject);
    }

    /**
     * 新增项目
     *
     * @param tblProject 项目
     * @return 结果
     */
    @Override
    public int insertTblProject(TblProject tblProject) {
        tblProject.setCreateTime(DateUtils.getNowDate());
        return tblProjectMapper.insertTblProject(tblProject);
    }

    /**
     * 修改项目
     *
     * @param tblProject 项目
     * @return 结果
     */
    @Override
    public int updateTblProject(TblProject tblProject) {
        tblProject.setUpdateTime(DateUtils.getNowDate());
        tblProjectCostMapper.updateTblProjectCostName(tblProject);
        return tblProjectMapper.updateTblProject(tblProject);
    }

    /**
     * 删除项目对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTblProjectByIds(String ids) {
        return tblProjectMapper.deleteTblProjectByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除项目信息
     *
     * @param id 项目ID
     * @return 结果
     */
    @Override
    public int deleteTblProjectById(String id) {
        return tblProjectMapper.deleteTblProjectById(id);
    }

    @Override
    public Map<String, Object> selectTblUserProjectList(Long userId) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("list", tblProjectMapper.selectTblProjectList(new TblProject()));
        Map<String, Boolean> mine = new HashMap<String, Boolean>();
        for (String project : tblUserProjectMapper.selectTblUserProjectByUser(userId)) {
            mine.put(project, true);
        }
        result.put("mine", mine);
        return result;
    }

    @Override
    public Map<String, Object> selectTblProjectUserList(String id) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("list", userService.selectAvailableUserList(new SysUser()));
        Map<Long, Boolean> mine = new HashMap<Long, Boolean>();
        for (Long user : tblUserProjectMapper.selectTblUserProjectByProject(id)) {
            mine.put(user, true);
        }
        result.put("mine", mine);
        return result;
    }

    @Override
    public void insertAuthProject(Long userId, String[] mine) {
        tblUserProjectMapper.deleteTblUserProjectByUser(userId);
        if (mine.length == 1 && mine[0].equalsIgnoreCase(TblUserProjectMapper.Clear_All)) {
            return;
        } else if (mine.length == 1 && mine[0].equalsIgnoreCase(TblUserProjectMapper.Insert_ALL)) {
            tblUserProjectMapper.insertTblUserProjectAllProject(userId);
        } else if (mine.length > 0) {
            List<TblUserProject> list = new ArrayList<TblUserProject>();
            for (String projectId : mine) {
                TblUserProject project = new TblUserProject();
                project.setUserId(userId);
                project.setProjectId(projectId);
                list.add(project);
            }
            tblUserProjectMapper.insertTblUserProject(list);
        }
    }

    @Override
    public void insertAuthUser(String projectId, String[] mine) {
        tblUserProjectMapper.deleteTblUserProjectByProject(projectId);
        if (mine.length == 1 && mine[0].equalsIgnoreCase(TblUserProjectMapper.Clear_All)) {
            return;
        } else if (mine.length == 1 && mine[0].equalsIgnoreCase(TblUserProjectMapper.Insert_ALL)) {
            tblUserProjectMapper.insertTblUserProjectAllUser(projectId);
        } else if (mine.length > 0) {
            List<TblUserProject> list = new ArrayList<TblUserProject>();
            for (String userId : mine) {
                TblUserProject project = new TblUserProject();
                project.setUserId(Long.valueOf(userId));
                project.setProjectId(projectId);
                list.add(project);
            }
            tblUserProjectMapper.insertTblUserProject(list);
        }
    }

    @Override
    public List<TblProject> selectMyTblProjectList(Long userId) {
        return tblProjectMapper.selectMyTblProjectList(userId);
    }

    @Override
    public List<TblProject> selectPMOTblProjectList(Long userId) {
        UserIDQuery query = new UserIDQuery();
        query.setUserId(userId);
        return tblProjectMapper.selectPMOTblProjectList(query);
    }

    @Override
    public List<TblProject> selectTblProjectListByUserProject(Long userId) {
        return tblProjectMapper.selectTblProjectListByUserProject(userId);
    }

}
