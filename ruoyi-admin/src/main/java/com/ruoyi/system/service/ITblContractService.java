package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.ContractSettleIndexModel;
import com.ruoyi.system.domain.TblContract;

/**
 * 合同Service接口
 * 
 * @author ruoyi
 * @date 2022-02-24
 */
public interface ITblContractService 
{
    /**
     * 查询合同
     * 
     * @param id 合同ID
     * @return 合同
     */
    public TblContract selectTblContractById(String id);

    /**
     * 查询合同列表
     * 
     * @param tblContract 合同
     * @return 合同集合
     */
    public List<TblContract> selectTblContractList(TblContract tblContract);

    /**
     * 查询合同列表
     *
     * @param tblContract 合同
     * @return 合同集合
     */
    public List<ContractSettleIndexModel> selectTblContractSettleList(TblContract tblContract);

    /**
     * 新增合同
     * 
     * @param tblContract 合同
     * @return 结果
     */
    public int insertTblContract(TblContract tblContract);

    /**
     * 修改合同
     * 
     * @param tblContract 合同
     * @return 结果
     */
    public int updateTblContract(TblContract tblContract);

    /**
     * 批量删除合同
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTblContractByIds(String ids);

    /**
     * 删除合同信息
     * 
     * @param id 合同ID
     * @return 结果
     */
    public int deleteTblContractById(String id);
}
