package com.ruoyi.system.domain.excel;

import com.ruoyi.common.annotation.Excel;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CostDetailForExcel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Excel(name = "名称")
    private String name;
    @Excel(name = "内容", autoSize = true)
    private String remark;
    @Excel(name = "时间")
    private String timeSpan;
    @Excel(name = "工时", isStatistics = true)
    private String cost;

    public String getName() {
        return name;
    }

    public String getRemark() {
        return remark;
    }

    public String getTimeSpan() {
        return timeSpan;
    }

    public String getCost() {
        return cost;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRemark(String remark) {
        if (remark == null) {
            return;
        }
        this.remark = remark.replaceAll("<BR>", String.valueOf((char) 10));
    }

    public void setTimeSpan(Date sd, Date ed) {
        if (sd == null || ed == null) {
            this.timeSpan = "-";
        } else {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            this.timeSpan = format.format(sd) + "~" + format.format(ed);
        }

    }

    public void setCost(BigDecimal cost) {
        if (cost == null || "".equals(cost)) {
            this.cost = "-";
            return;
        }
        if (new BigDecimal(cost.intValue()).compareTo(cost) == 0) {
            this.cost = String.valueOf(cost.intValue());
        } else {
            this.cost = String.format("%.2f", cost);
        }
    }
}
