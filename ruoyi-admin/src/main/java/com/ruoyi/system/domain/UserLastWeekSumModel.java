package com.ruoyi.system.domain;

import org.apache.poi.hpsf.Decimal;

import java.io.Serializable;
import java.math.BigDecimal;

public class UserLastWeekSumModel implements Serializable {
    private Long userCount;
    private Long reportedCount;
    private BigDecimal costAmount;
    private BigDecimal twoWeeksAgoAmount;

    public Long getUserCount() {
        return this.userCount;
    }

    public Long getReportedCount() {
        return this.reportedCount;
    }

    public BigDecimal getCostAmount() {
        return this.costAmount != null ? this.costAmount : BigDecimal.ZERO;
    }

    public BigDecimal getTwoWeeksAgoAmount(){
        return this.twoWeeksAgoAmount != null ? this.twoWeeksAgoAmount : BigDecimal.ZERO;
    }

    public void setUserCount(Long userCount) {
        this.userCount = userCount;
    }

    public void setReportedCount(Long reportedCount) {
        this.reportedCount = reportedCount;
    }

    public void setCostAmount(BigDecimal costAmount) {
        this.costAmount = costAmount;
    }

    public void setTwoWeeksAgoAmount(BigDecimal twoWeeksAgoAmount){
        this.twoWeeksAgoAmount = twoWeeksAgoAmount;
    }

}
