package com.ruoyi.system.domain.query;

import com.ruoyi.common.core.domain.BaseEntity;

public class UserIDQuery extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private  Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
