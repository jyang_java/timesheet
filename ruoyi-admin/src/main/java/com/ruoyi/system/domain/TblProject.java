package com.ruoyi.system.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.web.domain.server.Sys;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 项目对象 tbl_project
 *
 * @author ruoyi
 * @date 2022-02-28
 */
public class TblProject extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 自增编号
     */
    private Long autoId;

    /**
     * 项目编号，根据自增编号转换
     */
    private String projectNo;

    /**
     * 名称
     */
    @Excel(name = "名称")
    private String name;

    /**
     * 负责人id
     */
    private Long leaderId;

    /**
     * 负责人
     */
    @Excel(name = "负责人")
    private String leader;

    /**
     * 状态（0正常 1停用）
     */
    @Excel(name = "状态", readConverterExp = "1=正常,2=已完成,3=已停止")
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 合同id
     */
    private String contractId;

    /**
     * 合同名称
     */
    @Excel(name = "合同名称")
    private String contractName;

    /**
     * 甲方
     */
    @Excel(name = "甲方")
    private String buyers;

    /**
     * 乙方
     */
    @Excel(name = "乙方")
    private String sellers;

    /**
     * 起始日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setAutoId(Long autoId) {
        this.autoId = autoId;
        this.projectNo = this.autoId != null ? String.format("%08d", this.autoId.longValue()) : "-";
    }

    public Long getAutoId() {
        return this.autoId;
    }

    public String getProjectNo() {
        return projectNo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLeaderId(Long leaderId) {
        this.leaderId = leaderId;
    }

    public Long getLeaderId() {
        return leaderId;
    }

    public void setLeader(String leader) {
        this.leader = leader != "--请选择--" ? leader : "";
    }

    public String getLeader() {
        return leader;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName != "--请选择--" ? contractName : "";
    }

    public String getContractName() {
        return contractName;
    }

    public void setBuyers(String buyers) {
        this.buyers = buyers;
    }

    public String getBuyers() {
        return buyers;
    }

    public void setSellers(String sellers) {
        this.sellers = sellers;
    }

    public String getSellers() {
        return sellers;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public TblProject setOrder(Long userId, String createBy) {
        StringBuffer order = new StringBuffer("order by ");
        if (userId != null) {
            this.getParams().put("user", userId);
            order.append(
                    "(select max(create_time) from tbl_project_cost tpc where tpc.project_id=tbl_project.id and tpc.user=#{params.user}) desc,");
        }
        this.setCreateBy(createBy);
        order.append(
                        "(select max(create_time) from tbl_project_cost tpc1 where tpc1.project_id=tbl_project.id and tpc1.create_by=#{createBy}) desc,")
                .append("if(create_by =#{createBy},create_time,null) desc,create_time desc");
        this.getParams().put("order", order.toString());
        return this;
    }

    public TblProject setOrder() {
        this.getParams().put("order", "order by CONVERT(name using gbk)");
        return this;
    }

    public TblProject setOrderByCreatTime(Boolean desc) {
        this.getParams().put("order", "order by create_time" + (desc ? " desc" : ""));
        return this;
    }


    private String projectNoQuery;

    public String getProjectNoQuery() {
        return projectNoQuery;
    }

    public void setProjectNoQuery(String projectNoQuery) {
        this.projectNoQuery = projectNoQuery;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("remark", getRemark())
                .append("leaderId", getLeaderId())
                .append("leader", getLeader())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("contractId", getContractId())
                .append("createBy", getCreateBy())
                .append("contractName", getContractName())
                .append("createTime", getCreateTime())
                .append("buyers", getBuyers())
                .append("sellers", getSellers())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("startDate", getStartDate())
                .append("endDate", getEndDate())
                .toString();
    }
}
