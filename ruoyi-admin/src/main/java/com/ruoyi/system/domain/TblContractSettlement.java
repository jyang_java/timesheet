package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 合同收款对象 tbl_contract_settlement
 *
 * @author ruoyi
 * @date 2022-03-01
 */
public class TblContractSettlement extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 合同id
     */
    @Excel(name = "合同id")
    private String contractId;

    /**
     * 计划收款or确认收款
     */
    @Excel(name = "计划收款or确认收款")
    private String settleType;

    /**
     * 收款日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "收款日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date settleDate;

    /**
     * 金额
     */
    @Excel(name = "金额")
    private BigDecimal amount;

    /**
     * 付款方式
     */
    @Excel(name = "付款方式")
    private String payMode;

    /**
     * 提示标识
     */
    @Excel(name = "提示标识")
    private String warnFlag;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setSettleType(String settleType) {
        this.settleType = settleType;
    }

    public String getSettleType() {
        return settleType;
    }

    public void setSettleDate(Date settleDate) {
        this.settleDate = settleDate;
    }

    public Date getSettleDate() {
        return settleDate;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setWarnFlag(String warnFlag) {
        this.warnFlag = warnFlag;
    }

    public String getWarnFlag() {
        return warnFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("contractId", getContractId())
                .append("settleType", getSettleType())
                .append("settleDate", getSettleDate())
                .append("amount", getAmount())
                .append("payMode", getPayMode())
                .append("remark", getRemark())
                .append("warnFlag", getWarnFlag())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
