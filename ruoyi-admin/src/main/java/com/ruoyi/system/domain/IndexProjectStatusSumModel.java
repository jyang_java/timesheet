package com.ruoyi.system.domain;

public class IndexProjectStatusSumModel {
    private String statusCode;
    private Long count;

    public String getStatusCode() {
        return this.statusCode;
    }

    public Long getCount() {
        return this.count;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
