package com.ruoyi.system.domain.query;

import java.io.Serializable;
import java.util.Date;

public class ProjectCostExportQuery implements Serializable {
    private static final long serialVersionUID = 1L;
    private String sumType;
    private String queryId;
    private Date startDate;
    private Date endDate;

    public String getSumType() {
        return sumType;
    }

    public String getQueryId() {
        return queryId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setSumType(String sumType) {
        this.sumType = sumType;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
