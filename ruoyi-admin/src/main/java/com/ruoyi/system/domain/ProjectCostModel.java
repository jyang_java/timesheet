package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 工时信息
 * 
 * @author ruoyi
 */
public class ProjectCostModel {

	/** 项目ID */
	private String[] projectId;

	/** 组ID */
	private String groupId;

	/** 用户ID */
	private Long user;

	/** 起始日期 */
	private Date startDate;

	/** 结束日期 */
	private Date endDate;

	/** 人时 */
	private BigDecimal[] costDay;

    /** 备注 */
    private String[] remark;

	public String[] getRemark() {
		return remark;
	}

	public void setRemark(String[] remark) {
		this.remark = remark;
	}

	public String[] getProjectId() {
		return projectId;
	}

	public void setProjectId(String[] projectId) {
		this.projectId = projectId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public BigDecimal[] getCostDay() {
		return costDay;
	}

	public void setCostDay(BigDecimal[] costDay) {
		this.costDay = costDay;
	}



}
