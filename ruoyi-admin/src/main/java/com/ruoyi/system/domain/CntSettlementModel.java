package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;

public class CntSettlementModel {
    private String contractId;
    private String settleType;
    private Date[] settleDate;
    private BigDecimal[] amount;
    private String[] remark;

    public String getContractId() {
        return this.contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getSettleType() {
        return this.settleType;
    }

    public void setSettleType(String settleType) {
        this.settleType = settleType;
    }

    public Date[] getSettleDate() {
        return this.settleDate;
    }

    public void setSettleDate(Date[] settleDate) {
        this.settleDate = settleDate;
    }

    public BigDecimal[] getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal[] amount) {
        this.amount = amount;
    }

    public String[] getRemark() {
        return this.remark;
    }

    public void setRemark(String[] remark) {
        this.remark = remark;
    }
}
