package com.ruoyi.system.domain;

import org.apache.poi.hpsf.Decimal;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProjectSumModel implements Serializable {
    private String projectId;
    private String projectName;
    private BigDecimal costAmount;
    private BigDecimal twoWeeksAgoAmount;

    public String getProjectId() {
        return this.projectId;
    }

    public String getProjectName() {
        return this.projectName;
    }

    public BigDecimal getCostAmount() {
        return this.costAmount != null ? this.costAmount : BigDecimal.ZERO;
    }

    public BigDecimal getTwoWeeksAgoAmount() {
        return this.twoWeeksAgoAmount != null ? this.twoWeeksAgoAmount : BigDecimal.ZERO;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setCostAmount(BigDecimal costAmount) {
        this.costAmount = costAmount;
    }

    public void setTwoWeeksAgoAmount(BigDecimal twoWeeksAgoAmount) {
        this.twoWeeksAgoAmount = twoWeeksAgoAmount;
    }
}
