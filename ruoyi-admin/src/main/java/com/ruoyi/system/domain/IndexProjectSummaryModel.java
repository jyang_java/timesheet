package com.ruoyi.system.domain;

public class IndexProjectSummaryModel {
    private String name;
    private Long count;

    public String getName() {
        return this.name;
    }

    public Long getCount() {
        return this.count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
