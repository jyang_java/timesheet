package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工时对象 tbl_project_cost
 *
 * @author ruoyi
 * @date 2021-02-18
 */
public class TblProjectCost extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 项目ID
     */
    private String projectId;

    /**
     * 组ID
     */
    private String groupId;

    /**
     * 项目名称
     */
    @Excel(name = "项目名称")
    private String projectName;

    /**
     * 用户ID
     */
    private Long user;

    /**
     * 用户名
     */
    @Excel(name = "用户名")
    private String userName;

    /**
     * 起始日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 人时
     */
    @Excel(name = "人时")
    private BigDecimal costDay;

    public TblProjectCost(String id) {
        this.id = id;
    }

    public TblProjectCost() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName != "--请选择--" ? projectName : "";
    }

    public String getProjectName() {
        return projectName;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Long getUser() {
        return user;
    }

    public void setUserName(String userName) {
        this.userName = userName != "--请选择--" ? userName : "";
    }

    public String getUserName() {
        return userName;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setCostDay(BigDecimal costDay) {
        this.costDay = costDay;
    }

    public BigDecimal getCostDay() {
        return costDay;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("projectId", getProjectId())
                .append("groupId", getGroupId())
                .append("projectName", getProjectName())
                .append("user", getUser())
                .append("userName", getUserName())
                .append("startDate", getStartDate())
                .append("endDate", getEndDate())
                .append("costDay", getCostDay())
                .append("remark", getRemark())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
