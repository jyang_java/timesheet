package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 合同对象 tbl_contract
 *
 * @author ruoyi
 * @date 2022-02-24
 */
public class TblContract extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 合同名称
     */
    @Excel(name = "合同名称")
    private String name;

    /**
     * 负责人id
     */
    @Excel(name = "负责人id")
    private Long leaderId;

    /**
     * 负责人
     */
    @Excel(name = "负责人")
    private String leader;

    /**
     * 甲方
     */
    @Excel(name = "甲方")
    private String buyers;

    /**
     * 乙方
     */
    @Excel(name = "乙方")
    private String sellers;

    /**
     * 签约日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "签约日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date signDate;

    /**
     * 起始日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 金额
     */
    @Excel(name = "金额")
    private BigDecimal amount;

    /**
     * 付款方式
     */
    @Excel(name = "付款方式")
    private String payMode;

    /**
     * 收款状态
     */
    @Excel(name = "收款状态")
    private Long payStatus;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLeaderId(Long leaderId) {
        this.leaderId = leaderId;
    }

    public Long getLeaderId() {
        return leaderId;
    }

    public void setLeader(String leader) {
        this.leader = leader != "--请选择--" ? leader : "";
    }

    public String getLeader() {
        return leader;
    }

    public void setBuyers(String buyers) {
        this.buyers = buyers;
    }

    public String getBuyers() {
        return buyers;
    }

    public void setSellers(String sellers) {
        this.sellers = sellers;
    }

    public String getSellers() {
        return sellers;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayStatus(Long payStatus) {
        this.payStatus = payStatus;
    }

    public Long getPayStatus() {
        return payStatus;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("leaderId", getLeaderId())
                .append("leader", getLeader())
                .append("buyers", getBuyers())
                .append("sellers", getSellers())
                .append("signDate", getSignDate())
                .append("startDate", getStartDate())
                .append("endDate", getEndDate())
                .append("amount", getAmount())
                .append("payMode", getPayMode())
                .append("remark", getRemark())
                .append("payStatus", getPayStatus())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
