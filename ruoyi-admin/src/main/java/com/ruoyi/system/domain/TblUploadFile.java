package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文件关联对象 tbl_upload_file
 * 
 * @author ruoyi
 * @date 2022-03-08
 */
public class TblUploadFile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 关联类型 */
    @Excel(name = "关联类型")
    private String relType;

    /** 关联表ID */
    @Excel(name = "关联表ID")
    private String relId;

    /** 文件名 */
    @Excel(name = "文件名")
    private String fileName;

    /** 文件路径（包含保存文件名） */
    @Excel(name = "文件路径", readConverterExp = "包=含保存文件名")
    private String filePath;

    /** 后缀名 */
    @Excel(name = "后缀名")
    private String fileExt;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setRelType(String relType) 
    {
        this.relType = relType;
    }

    public String getRelType() 
    {
        return relType;
    }
    public void setRelId(String relId) 
    {
        this.relId = relId;
    }

    public String getRelId() 
    {
        return relId;
    }
    public void setFileName(String fileName) 
    {
        this.fileName = fileName;
    }

    public String getFileName() 
    {
        return fileName;
    }
    public void setFilePath(String filePath) 
    {
        this.filePath = filePath;
    }

    public String getFilePath() 
    {
        return filePath;
    }
    public void setFileExt(String fileExt) 
    {
        this.fileExt = fileExt;
    }

    public String getFileExt() 
    {
        return fileExt;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("relType", getRelType())
            .append("relId", getRelId())
            .append("fileName", getFileName())
            .append("filePath", getFilePath())
            .append("fileExt", getFileExt())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .toString();
    }
}
