package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

public class ContractSettleIndexModel extends TblContract {

    /**
     * 已付款金额
     */
    @Excel(name = "已付款金额")
    private BigDecimal payedAmount;

    /**
     * 近期到期金额
     */
    @Excel(name = "近期到期金额")
    private BigDecimal matureAmount;

    /**
     * 近期到期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "近期到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date matureDate;

    public BigDecimal getPayedAmount() {
        return this.payedAmount;
    }

    public void setPayedAmount(BigDecimal payedAmount) {
        this.payedAmount = payedAmount;
    }

    public BigDecimal getMatureAmount() {
        return this.matureAmount;
    }

    public void setMatureAmount(BigDecimal matureAmount) {
        this.matureAmount = matureAmount;
    }

    public Date getMatureDate() {
        return this.matureDate;
    }

    public void setMatureDate(Date matureDate) {
        this.matureDate = matureDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("leaderId", getLeaderId())
                .append("leader", getLeader())
                .append("buyers", getBuyers())
                .append("sellers", getSellers())
                .append("signDate", getSignDate())
                .append("startDate", getStartDate())
                .append("endDate", getEndDate())
                .append("amount", getAmount())
                .append("payMode", getPayMode())
                .append("remark", getRemark())
                .append("payStatus", getPayStatus())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("payedAmount", getPayedAmount())
                .append("matureAmount", getMatureAmount())
                .append("matureDate", getMatureDate())
                .toString();
    }
}