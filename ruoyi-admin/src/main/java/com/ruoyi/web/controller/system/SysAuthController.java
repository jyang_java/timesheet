package com.ruoyi.web.controller.system;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.config.ThirdAuthConfig;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.AuthUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.shiro.auth.LoginType;
import com.ruoyi.framework.shiro.auth.UserToken;
import com.ruoyi.system.domain.SysAuthUser;
import com.ruoyi.system.mapper.SysUserMapper;
import me.zhyd.oauth.cache.AuthDefaultStateCache;
import me.zhyd.oauth.cache.AuthStateCache;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.model.AuthUser;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.utils.AuthStateUtils;

/**
 * 第三方认证授权处理
 *
 * @author ruoyi
 */
@Controller
public class SysAuthController extends BaseController {
    public static final String THIRD_WECHAT = "wechat";

    private AuthStateCache authStateCache;

    @Autowired
    private SysUserMapper userMapper;


    private final static Map<String, String> auths = new HashMap<String, String>();

    {
//        auths.put("gitee", "{\"clientId\":\"e5cf90f79b02128d8f140d6898c480ea9dc5278e15a23617e38b58de8ebdc8be\",\"clientSecret\":\"c02d1df684909ba465cf521d94209e2a50e45f2199244f2af697b514efb38d60\",\"redirectUri\":\"http://127.0.0.1:80/auth/callback/gitee\"}");
//        auths.put("github", "{\"clientId\":\"Iv1.1be0cdcd71aca63b\",\"clientSecret\":\"0d59d28b43152bc8906011624db37b0fed88d154\",\"redirectUri\":\"http://127.0.0.1:80/auth/callback/github\"}");
//        Serializable weChatConfig = new Serializable(){
//            public String clientId = ThirdAuthConfig.getWeChatAppId();
//            public String clientSecret = ThirdAuthConfig.getWeChatAppSecret();
//            public String redirectUri = ThirdAuthConfig.getWeChatCallBack();
//            public String scope = ThirdAuthConfig.getWeChatScope();
//        };
//        auths.put("wechat",JSONObject.toJSONString(weChatConfig));
        authStateCache = AuthDefaultStateCache.INSTANCE;
    }

    @PostMapping("/auth/qrcodeauth")
    @ResponseBody
    public AjaxResult qrCodeAuth(@RequestBody String source) {
        ThirdAuthConfig.ThirdConfig _config = ThirdAuthConfig.getSourceConfig(source);
        if (_config == null) {
            return AjaxResult.error("第三方登录方式暂不支持！");
        }
        AuthRequest authRequest = AuthUtils.getAuthRequest(source, _config.getAppId(), _config.getAgentId(), _config.getSecret(), _config.getCallBack(), authStateCache);

        String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
        return AjaxResult.success("", authorizeUrl);
    }

    /**
     * 认证授权
     *
     * @param source
     * @throws IOException
     */
    @GetMapping("/auth/{source}")
    @ResponseBody
    public void renderAuth(@PathVariable("source") String source) throws IOException {
//        System.out.println(ThirdAuthConfig.getWeChatAppId());
        System.out.println(RuoYiConfig.getName());
//        System.out.println(RuoYiConfig.getWeChatAppId());


        String obj = auths.get(source);
        if (StringUtils.isEmpty(obj)) {
            return;
        }
        JSONObject json = JSONObject.parseObject(obj);
        AuthRequest authRequest = AuthUtils.getAuthRequest(source, json.getString("clientId"), json.getString("agentId"), json.getString("clientSecret"), json.getString("redirectUri"), authStateCache);
        String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
        ServletUtils.getResponse().sendRedirect(authorizeUrl);
    }

    @GetMapping("/auth/callback/{source}")
    public String callBack(@PathVariable("source") String source, AuthCallback callback, HttpServletRequest request, ModelMap mmap) {
        mmap.put("source", source);
        mmap.put("callback", callback);
        return "success/callback";
    }

    /**
     * 回调结果
     */
    @SuppressWarnings("unchecked")
    @PostMapping("/auth/callback/{source}")
    public Object callbackAuth(@PathVariable("source") String source, AuthCallback callback, HttpServletRequest request) {
        ThirdAuthConfig.ThirdConfig _config = ThirdAuthConfig.getSourceConfig(source);
        if (_config == null) {
            return new ModelAndView("error/auth/support");
        }

//        JSONObject json = JSONObject.parseObject(obj);
        AuthRequest authRequest = AuthUtils.getAuthRequest(source, _config.getAppId(), _config.getAgentId(), _config.getSecret(), _config.getCallBack(), authStateCache);
        AuthResponse<AuthUser> response = authRequest.login(callback);
        if (response.ok()) {
            if (SecurityUtils.getSubject() != null && SecurityUtils.getSubject().getPrincipal() != null) {
                SysUser user = userMapper.selectAuthUserByUuid(source + response.getData().getUuid());
                if (StringUtils.isNotNull(user)) {
                    return new ModelAndView("error/auth/error");
                }
                // 若已经登录则直接绑定系统账号
                SysAuthUser authUser = new SysAuthUser();
                authUser.setAvatar(response.getData().getAvatar());
                authUser.setUuid(source + response.getData().getUuid());
                authUser.setUserId(ShiroUtils.getUserId());
                authUser.setUserName(response.getData().getNickname());
                authUser.setLoginName(response.getData().getUsername());
                authUser.setEmail(response.getData().getEmail());
                authUser.setSource(source);
                userMapper.insertAuthUser(authUser);
                return new ModelAndView("success/auth");
            }
            SysUser user = userMapper.selectAuthUserByUuid(source + response.getData().getUuid());
            if (StringUtils.isNotNull(user)) {
                Subject subject = SecurityUtils.getSubject();
                UserToken token = new UserToken(user.getLoginName(), LoginType.NOPASSWD);
                subject.login(token);
                return new ModelAndView("success/auth");
            } else {
                return new ModelAndView("error/auth/bind");
            }
        }
        return new ModelAndView("error/auth/unkown");
    }

    /**
     * 检查是否授权
     */
    @PostMapping("/auth/checkAuthUser")
    @ResponseBody
    public AjaxResult checkAuthUser(SysAuthUser authUser) {
        Long userId = ShiroUtils.getUserId();
        String source = authUser.getSource();
        if (userMapper.checkAuthUser(userId, source) > 0) {
            return error(source + "平台账号已经绑定");
        }
        return AjaxResult.success();
    }

    /**
     * 取消授权
     */
    @PostMapping("/auth/unlock")
    @ResponseBody
    public AjaxResult unlockAuth(SysAuthUser authUser) {
        return toAjax(userMapper.deleteAuthUser(authUser.getAuthId()));
    }

}
