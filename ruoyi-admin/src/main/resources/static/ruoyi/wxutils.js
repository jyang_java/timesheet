(function (window, document, undefined) {
    function WxLogin(data) {
        var self_redirect = 'default'
        if (data.self_redirect === true) {
            self_redirect = 'true'
        } else if (data.self_redirect === false) {
            self_redirect = 'false'
        }

        var frame = document.createElement("iframe");
        // var url = "https://open.weixin.qq.com/connect/qrconnect?appid=" + data.appid + "&scope=" + data.scope + "&redirect_uri=" + data.redirect_uri + "&state=" + data.state + "&login_type=jssdk" + "&self_redirect=" + self_redirect;
        var url = data.url;
        url += data.style ? ("&style=" + data.style) : "";
        url += data.href ? ("&href=" + data.href) : "";
        url += "&login_type=jssdk" + "&self_redirect=" + self_redirect;
        url += "&version=1.2.5";
        frame.src = url;
        frame.frameBorder = "0";
        frame.allowTransparency = "true";
        frame.scrolling = "no";
        frame.width = "233px";
        frame.height = "300px";
        var el = document.getElementById(data.id);
        el.innerHTML = "";
        el.appendChild(frame);
    }
    function WxQRCodeUrl(data){
        var self_redirect = 'default'
        if (data.self_redirect === true) {
            self_redirect = 'true'
        } else if (data.self_redirect === false) {
            self_redirect = 'false'
        }
        var frame = document.createElement("iframe");
        var url = data.url;
        url += data.style ? ("&style=" + data.style) : "";
        url += data.href ? ("&href=" + data.href) : "";
        url += "&login_type=jssdk" + "&self_redirect=" + self_redirect;
        url += "&version=1.2.5";
        return url;
    }
    window.WxLogin = WxLogin;
    window.WxQRCodeUrl = WxQRCodeUrl;
})(window, document);
