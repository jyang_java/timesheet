package com.ruoyi.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@ConfigurationProperties(prefix = "thirdauth")
public class ThirdAuthConfig {
    private static String[] supportSources = new String[]{"wechat","wechat_enterprise"};
    private static String weChatAppId;
    private static String weChatAppSecret;
    private static String weChatCallBack;
    private static String weChatEnterpriseCorpID;
    private static String weChatEnterpriseAgentId;
    private static String weChatEnterpriseSecret;
    private static String weChatEnterpriseCallBack;

    public static String getWeChatAppId() {
        return weChatAppId;
    }

    public static String getWeChatAppSecret() {
        return weChatAppSecret;
    }

    public static String getWeChatCallBack() {
        return weChatCallBack;
    }

    public static String[] getSupportSources() {
        return supportSources;
    }

    public static String getWeChatEnterpriseCorpID() {
        return weChatEnterpriseCorpID;
    }

    public static String getWeChatEnterpriseAgentId() {
        return weChatEnterpriseAgentId;
    }

    public static String getWeChatEnterpriseSecret() {
        return weChatEnterpriseSecret;
    }

    public static String getWeChatEnterpriseCallBack() {
        return weChatEnterpriseCallBack;
    }

    public static ThirdConfig getSourceConfig(String source){
        ThirdConfig _config = null;
        switch (source)
        {
            case "wechat":
                _config = new ThirdConfig();
                _config.setSource("wechat");
                _config.setAppId(weChatAppId);
                _config.setSecret(weChatAppSecret);
                _config.setCallBack(weChatCallBack);
                break;
            case "wechat_enterprise":
                _config = new ThirdConfig();
                _config.setSource("wechat_enterprise");
                _config.setAppId(weChatEnterpriseCorpID);
                _config.setAgentId(weChatEnterpriseAgentId);
                _config.setSecret(weChatEnterpriseSecret);
                _config.setCallBack(weChatEnterpriseCallBack);
            default:
                break;
        }
        return _config;
    }

    public void setWeChatAppId(String weChatAppId) {
        ThirdAuthConfig.weChatAppId = weChatAppId;
    }

    public void setWeChatAppSecret(String weChatAppSecret) {
        ThirdAuthConfig.weChatAppSecret = weChatAppSecret;
    }

    public void setWeChatCallBack(String weChatCallBack) {
        ThirdAuthConfig.weChatCallBack = weChatCallBack;
    }

    public void setWeChatEnterpriseCorpID(String weChatEnterpriseCorpID) {
        ThirdAuthConfig.weChatEnterpriseCorpID = weChatEnterpriseCorpID;
    }

    public void setWeChatEnterpriseAgentId(String weChatEnterpriseAgentId) {
        ThirdAuthConfig.weChatEnterpriseAgentId = weChatEnterpriseAgentId;
    }

    public void setWeChatEnterpriseSecret(String weChatEnterpriseSecret) {
        ThirdAuthConfig.weChatEnterpriseSecret = weChatEnterpriseSecret;
    }

    public void setWeChatEnterpriseCallBack(String weChatEnterpriseCallBack) {
        ThirdAuthConfig.weChatEnterpriseCallBack = weChatEnterpriseCallBack;
    }

    public static class ThirdConfig{
        private String source;
        private String appId;
        private String agentId;
        private String secret;
        private String callBack;

        public String getSource() {
            return source;
        }

        public String getAppId() {
            return appId;
        }

        public String getAgentId() {
            return agentId;
        }

        public String getSecret() {
            return secret;
        }


        public String getCallBack() {
            return callBack;
        }
        public void setSource(String source) {
            this.source = source;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public void setAgentId(String agentId) {
            this.agentId = agentId;
        }

        public void setSecret(String secret) {
            this.secret = secret;
        }

        public void setCallBack(String callBack) {
            this.callBack = callBack;
        }
    }

}
